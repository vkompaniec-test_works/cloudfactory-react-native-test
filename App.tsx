import React from 'react';

import {AppNavigation} from './src/navigation/AppNavigation';
import {RootStoreProvider} from './src/stores/RootStoreContext';

import Toast from 'react-native-toast-message';

function App(): JSX.Element {
  return (
    <RootStoreProvider>
      <AppNavigation />
      <Toast />
    </RootStoreProvider>
  );
}

export default App;
