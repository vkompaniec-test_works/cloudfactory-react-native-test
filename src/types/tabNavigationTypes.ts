import {StocksRoutes, TabRoutes} from '../navigation/routes';
import type {NativeStackScreenProps} from '@react-navigation/native-stack';

export type TabNavigationRoutes = {
  [TabRoutes.STOCKS_STACK]: undefined;
  [TabRoutes.ABOUT]: undefined;
};

export type StocksStackRoutes = {
  [StocksRoutes.STOCKS]: undefined;
  [StocksRoutes.PAIR_INFO]: {name: string};
};

export type PairInfoScreenProps = NativeStackScreenProps<
  StocksStackRoutes,
  StocksRoutes.PAIR_INFO
>;
