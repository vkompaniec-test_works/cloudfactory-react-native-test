import {Toast} from 'react-native-toast-message/lib/src/Toast';

export const ErrorService = {
  handleError: (error: Error | string) => {
    const errorMessage = error instanceof Error ? error.message : error;
    Toast.show({
      type: 'error',
      text1: 'Ошибка',
      text2: errorMessage,
      visibilityTime: 2000,
    });
    //Тут делаем что-то еще с ошибкой (Sentry или еще что-то)
  },
};
