import React, {useEffect, useRef} from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';
import {observer} from 'mobx-react-lite';
import {useRootStore} from '@stores/RootStoreContext';
import {PairRow} from '@components/organisms/PairRow';
import {FullscreenLoading} from '@components/organisms/FullscreenLoading';
import {CurrenciesFilter} from '@components/molecules/CurrenciesFilter';
import {TabRoutes} from '../navigation/routes';

const StocksScreen = observer(({navigation, route}) => {
  const {
    stocksStore,
    navigationStore: {tabRoute},
  } = useRootStore();
  const interval = useRef<number>();

  const {stocks, stocksState, fetchStocks, getFilteredPairs} = stocksStore;

  useEffect(() => {
    fetchStocks();
  }, [fetchStocks]);

  useEffect(() => {
    if (tabRoute === TabRoutes.STOCKS_STACK) {
      interval.current = setInterval(() => {
        fetchStocks();
      }, 5000);
    }
    if (tabRoute === TabRoutes.ABOUT && interval.current) {
      clearInterval(interval.current);
    }
  }, [tabRoute, fetchStocks]);

  if (!stocks && stocksState === 'loading') {
    return <FullscreenLoading />;
  }

  if (stocksState === 'error') {
    return <View />;
  }

  return (
    <View style={styles.screen}>
      {getFilteredPairs && (
        <View>
          <CurrenciesFilter />
          <ScrollView>
            {Object.keys(getFilteredPairs).map(item => (
              <PairRow key={item} index={item} />
            ))}
          </ScrollView>
        </View>
      )}
    </View>
  );
});

export default StocksScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
