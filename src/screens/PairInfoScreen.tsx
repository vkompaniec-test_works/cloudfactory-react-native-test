import React, {useEffect} from 'react';
import {
  View,
  StyleSheet,
  Text,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import {Colors} from '../constants/colors';
import {PairInfoScreenProps} from '../types/tabNavigationTypes';
import {observer} from 'mobx-react-lite';
import {useRootStore} from '../stores/RootStoreContext';
import {sub, getTime} from 'date-fns';
import {CandlestickChart} from 'react-native-wagmi-charts';
import {toJS} from 'mobx';
import {useIsFocused} from '@react-navigation/native';
import {PairInfoRow} from '../components/atoms/PairInfoRow';
import {BackButton} from '../components/atoms/BackButton';

const from = getTime(
  sub(new Date(), {
    days: 1,
  }),
);
const to = getTime(new Date());

const PairInfoScreen = observer(({route, navigation}: PairInfoScreenProps) => {
  const {
    stocksStore: {
      stocks,
      fetchPairChartData,
      chartDataState,
      pairChartData,
      clearPairChartData,
    },
  } = useRootStore();
  const name = route.params.name;
  const pair = stocks?.[name];

  const isFocused = useIsFocused();

  useEffect(() => {
    navigation.setOptions({
      title: name,
      headerBackTitle: 'Stocks',
      headerLeft: () => <BackButton />,
    });
    fetchPairChartData(name, from, to, 1800);
  }, [name, navigation, fetchPairChartData]);

  useEffect(() => {
    if (!isFocused) {
      clearPairChartData();
    }
  }, [isFocused, clearPairChartData]);

  const percent = Number(pair?.percentChange);

  let priceColor = Colors.silverLakeBlue;

  if (percent < 0) {
    priceColor = Colors.tomato;
  }

  if (percent > 0) {
    priceColor = Colors.darkSpreengGreen;
  }

  return (
    <View style={styles.screen}>
      <ScrollView>
        <View style={styles.info}>
          <View style={styles.price}>
            <Text
              style={StyleSheet.flatten([
                styles.lastText,
                {color: priceColor},
              ])}>
              {pair?.highestBid}
            </Text>
          </View>
          <View style={styles.addInfo}>
            <View
              style={StyleSheet.flatten([styles.addContainer, styles.margin])}>
              <PairInfoRow title="24h High" value={pair?.high24hr || ''} />
              <PairInfoRow title="24h Low" value={pair?.low24hr || ''} />
            </View>
            <View style={styles.addContainer}>
              <PairInfoRow title="Highest Bid" value={pair?.highestBid || ''} />
              <PairInfoRow title="Lowest Ask" value={pair?.lowestAsk || ''} />
            </View>
          </View>
        </View>
        {chartDataState === 'loading' && (
          <View style={styles.chart}>
            <ActivityIndicator size="large" color={Colors.silverLakeBlue} />
          </View>
        )}
        {pairChartData && isFocused && (
          <View style={styles.chart}>
            <Text style={styles.chartTitle}>24 hours chart</Text>
            <CandlestickChart.Provider data={toJS(pairChartData)}>
              <CandlestickChart>
                <CandlestickChart.Candles />
              </CandlestickChart>
              <CandlestickChart.PriceText type="open" precision={2} />
              <CandlestickChart.PriceText type="high" />
              <CandlestickChart.PriceText type="low" />
              <CandlestickChart.PriceText type="close" />
              <CandlestickChart.DatetimeText
                locale="ru-RU"
                options={{
                  hour: 'numeric',
                  minute: 'numeric',
                  second: 'numeric',
                }}
              />
            </CandlestickChart.Provider>
          </View>
        )}
      </ScrollView>
    </View>
  );
});

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  info: {
    flexDirection: 'row',
    marginTop: 8,
  },
  lastText: {
    fontSize: 18,
    fontWeight: '600',
  },
  price: {
    flex: 2,
    marginLeft: 8,
  },

  chartTitle: {
    fontSize: 16,
    alignSelf: 'center',
    marginBottom: 16,
  },
  addInfo: {
    flex: 2,
  },
  addContainer: {
    flex: 2,
    flexDirection: 'row',
  },
  margin: {
    marginBottom: 16,
  },
  chart: {
    marginVertical: 32,
  },
});

export default PairInfoScreen;
