import React from 'react';
import {StyleSheet, View} from 'react-native';

const AboutScreen = () => {
  return <View style={styles.screen} />;
};

export default AboutScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
