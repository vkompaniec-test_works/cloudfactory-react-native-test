import httpClient from './httpClient';

export const StockApi = {
  getStocks: async <T>() => httpClient.get<T>('public?command=returnTicker'),
  getPairChartData: async <T>(
    pair: string,
    from: number,
    to: number,
    period: number,
  ) =>
    httpClient.get<T>(
      `public?command=returnChartData&currencyPair=${pair}&start=${from}&end=${to}&period=${period}`,
    ),
};
