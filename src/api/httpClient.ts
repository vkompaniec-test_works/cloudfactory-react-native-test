import axios from 'axios';

const httpClient = axios.create({
  baseURL: 'https://poloniex.com',
});

export default httpClient;
