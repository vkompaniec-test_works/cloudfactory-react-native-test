import React from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {FilterButton} from '../atoms/FilterButton';
import {useRootStore} from '../../stores/RootStoreContext';
import {observer} from 'mobx-react-lite';

export const CurrenciesFilter = observer(() => {
  const {
    stocksStore: {selectFilterGroup, selectedFilterGroup, currencies},
  } = useRootStore();

  const separator = () => <View style={styles.separator} />;

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.flatList}
        horizontal
        showsHorizontalScrollIndicator={false}
        data={currencies}
        initialNumToRender={20}
        keyExtractor={item => item}
        ItemSeparatorComponent={separator}
        renderItem={({item}) => (
          <FilterButton
            onPress={() => selectFilterGroup(item)}
            title={item}
            isSelected={item === selectedFilterGroup}
          />
        )}
      />
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    marginVertical: 16,
  },
  separator: {
    width: 16,
  },
  flatList: {
    paddingLeft: 16,
  },
});
