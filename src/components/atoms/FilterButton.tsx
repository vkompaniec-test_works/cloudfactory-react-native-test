import {observer} from 'mobx-react-lite';
import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Colors} from '../../constants/colors';

type FilterButtonProps = {
  onPress: () => void;
  title: string;
  isSelected: boolean;
};

export const FilterButton = observer(
  ({onPress, title, isSelected}: FilterButtonProps) => {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={StyleSheet.flatten([
          styles.container,
          {backgroundColor: isSelected ? Colors.honeydew : '#fff'},
        ])}>
        <Text>{title}</Text>
      </TouchableOpacity>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 4,
    paddingHorizontal: 8,
  },
});
