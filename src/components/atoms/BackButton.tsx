import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import ArrowBack from '@icons/arrow_back.svg';
import {useNavigation} from '@react-navigation/native';

export const BackButton = () => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity onPress={navigation.goBack} style={styles.container}>
      <ArrowBack />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 40,
    height: 40,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
});
