import React from 'react';
import StocksButton from '@icons/stocks.svg';
import AboutButton from '@icons/about.svg';
import {Colors} from '../../constants/colors';

export enum BottomTabButtonType {
  STOCKS,
  ABOUT,
}

type BottomTabButtonProps = {
  type: BottomTabButtonType;
  focused: boolean;
};

export const BottomTabButton = ({type, focused}: BottomTabButtonProps) => {
  const stroke = focused ? Colors.delftBlue : Colors.silverLakeBlue;

  const icon = {
    [BottomTabButtonType.STOCKS]: <StocksButton stroke={stroke} />,
    [BottomTabButtonType.ABOUT]: <AboutButton stroke={stroke} />,
  };

  return icon[type];
};
