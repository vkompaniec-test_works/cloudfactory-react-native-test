import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Colors} from '../../constants/colors';

type PairInfoRowProps = {
  title: string;
  value: string;
};

export const PairInfoRow = ({title, value}: PairInfoRowProps) => (
  <View style={styles.additionalInfo}>
    <Text style={styles.addTitleText}>{title}</Text>
    <Text style={styles.addText}>{value}</Text>
  </View>
);

const styles = StyleSheet.create({
  additionalInfo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  addText: {
    fontSize: 10,
    color: Colors.silverLakeBlue,
    fontWeight: '500',
  },
  addTitleText: {
    fontSize: 9,
  },
});
