import React from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {Colors} from '../../constants/colors';

export const FullscreenLoading = () => (
  <View style={styles.container}>
    <ActivityIndicator size="large" color={Colors.delftBlue} />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.white,
  },
});
