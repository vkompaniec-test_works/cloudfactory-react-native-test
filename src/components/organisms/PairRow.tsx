import {observer} from 'mobx-react-lite';
import React from 'react';
import {StyleSheet, View, Text, TouchableWithoutFeedback} from 'react-native';
import {useRootStore} from '@stores/RootStoreContext';
import {Colors} from '@constants/colors';

import {StocksRoutes} from '../../navigation/routes';
import {useNavigation} from '@react-navigation/native';

type PairRowProps = {
  index: string;
};
export const PairRow = observer(({index}: PairRowProps) => {
  const {
    stocksStore: {stocks},
  } = useRootStore();
  const navigation = useNavigation();

  if (!stocks) {
    return null;
  }

  const pair = stocks[index];
  const percent = Number(pair.percentChange);

  let percentBackground = Colors.silverLakeBlue;

  if (percent < 0) {
    percentBackground = Colors.tomato;
  }

  if (percent > 0) {
    percentBackground = Colors.darkSpreengGreen;
  }

  const handlePress = () => {
    navigation.navigate(StocksRoutes.PAIR_INFO, {name: index});
  };

  return (
    <TouchableWithoutFeedback onPress={handlePress}>
      <View style={styles.row}>
        <View style={styles.name}>
          <Text style={styles.nameText}>{index}</Text>
          <Text>{pair.quoteVolume}</Text>
        </View>
        <View style={styles.price}>
          <Text style={styles.highestBitText}>{pair.highestBid}</Text>
          <Text>{pair.high24hr}</Text>
        </View>
        <View style={styles.percent}>
          <View
            style={StyleSheet.flatten([
              styles.percentContainer,
              {backgroundColor: percentBackground},
            ])}>
            <Text style={styles.percentText}>
              {`${pair.percentChange.slice(0, 5)} %`}
            </Text>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
});

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginVertical: 8,
  },
  name: {
    flex: 3,
    marginLeft: 8,
  },
  price: {
    flex: 3,
    paddingLeft: 8,
    alignItems: 'flex-end',
  },
  percent: {
    flex: 2,
    alignItems: 'center',
    marginLeft: 8,
    paddingHorizontal: 4,
  },
  percentContainer: {
    width: '90%',
    alignItems: 'center',
    paddingVertical: 8,
  },
  nameText: {
    fontSize: 16,
    color: '#000',
    fontWeight: '600',
    marginBottom: 8,
  },
  highestBitText: {
    fontSize: 16,
    fontWeight: '600',
    color: Colors.delftBlue,
  },
  percentText: {
    color: '#fff',
    fontWeight: '500',
  },
});
