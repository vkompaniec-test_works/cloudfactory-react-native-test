import {Pair, PairChartData} from '../types/stocks';

export const isError = (
  data: Record<string, Pair> | PairChartData | {error: string},
): data is {error: string} => {
  return (data as {error: string}).error !== undefined;
};
