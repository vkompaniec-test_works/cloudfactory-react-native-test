import {Pair} from '../types/stocks';

export const filterByKey = (object: Record<string, Pair>, filterKey: string) =>
  Object.fromEntries(
    Object.entries(object).filter(
      ([key]) => key.includes(`_${filterKey}`) || key.includes(`${filterKey}_`),
    ),
  );
