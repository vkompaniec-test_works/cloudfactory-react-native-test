import {makeAutoObservable} from 'mobx';
import {TabRoutes} from '../navigation/routes';

class NavigationStore {
  tabRoute: TabRoutes | undefined = undefined;

  constructor() {
    makeAutoObservable(this);
  }

  setTabRoute = (route: TabRoutes) => {
    this.tabRoute = route;
  };
}

export default NavigationStore;
