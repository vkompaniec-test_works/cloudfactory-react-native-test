import {makeAutoObservable, runInAction} from 'mobx';
import {Pair, PairChartData} from '../types/stocks';
import RootStore from './RootStore';
import {StockApi} from '../api/stocksApi';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {isError} from '../utils/guards';
import {filterByKey} from '../utils/stocksUtils';
import {ErrorService} from '../services/errorService';

class StocksStore {
  rootStore: typeof RootStore;
  stocks: Record<string, Pair> | undefined = undefined;

  selectedFilterGroup: string | undefined = undefined;
  currencies: string[] | undefined = undefined;
  pairChartData: PairChartData | undefined = undefined;

  stocksState: 'init' | 'loading' | 'done' | 'error' = 'init';
  chartDataState: 'init' | 'loading' | 'done' | 'error' = 'init';

  constructor(rootStore: typeof RootStore) {
    makeAutoObservable(this);
    this.rootStore = rootStore;
  }

  changeStocksState = (state: 'init' | 'loading' | 'done' | 'error') => {
    this.stocksState = state;
  };

  changeChartDataState = (state: 'init' | 'loading' | 'done' | 'error') => {
    this.chartDataState = state;
  };

  clearPairChartData = () => (this.pairChartData = undefined);

  selectFilterGroup = (group: string) => {
    if (this.selectedFilterGroup === group) {
      this.selectedFilterGroup = undefined;
      return;
    }
    this.selectedFilterGroup = group;
  };

  get getFilteredPairs() {
    if (!this.selectedFilterGroup) {
      return this.stocks;
    }

    if (this.selectedFilterGroup && this.stocks) {
      return filterByKey(this.stocks, this.selectedFilterGroup);
    }
  }

  fetchPairChartData = async (
    pair: string,
    from: number,
    to: number,
    period: number,
  ) => {
    this.changeChartDataState('loading');
    try {
      const result = await StockApi.getPairChartData<PairChartData>(
        pair,
        from,
        to,
        period,
      );

      runInAction(() => {
        if (isError(result.data)) {
          ErrorService.handleError(result.data.error);
          this.changeChartDataState('done');
          return;
        }

        this.pairChartData = result.data.map(item => ({
          ...item,
          timestamp: item.date,
        }));

        this.changeChartDataState('done');
      });
    } catch (e) {
      ErrorService.handleError(e as Error);
      this.changeChartDataState('error');
    }
  };

  fetchStocks = async () => {
    this.changeStocksState('loading');
    try {
      const result = await StockApi.getStocks<
        Record<string, Pair> | {error: string}
      >();

      runInAction(() => {
        if (isError(result.data)) {
          ErrorService.handleError(result.data.error);
          this.changeStocksState('error');
          return;
        }
      });

      runInAction(() => {
        if (!this.stocks && !isError(result.data)) {
          this.stocks = result.data;
          this.changeStocksState('done');
          return;
        }
      });
      const keysArray = Object.keys(result?.data);

      runInAction(() => {
        if (!this.currencies) {
          const set = new Set<string>();
          keysArray.map(key => {
            const splitted = key.split('_');
            set.add(splitted[0]);
            set.add(splitted[1]);
          });
          this.currencies = Array.from(set);
        }
      });
      runInAction(() => {
        keysArray.map(key => {
          if (this.stocks && !isError(result.data)) {
            this.stocks[key] = result?.data[key];
          }
        });
      });
      this.changeStocksState('done');
    } catch (e) {
      const error = e as Error;
      ErrorService.handleError(error);
      this.changeStocksState('error');
    }
  };
}

export default StocksStore;
