import NavigationStore from './NavigationStore';
import StocksStore from './StocksStore';

class RootStore {
  stocksStore: StocksStore;
  navigationStore: NavigationStore;

  constructor() {
    this.stocksStore = new StocksStore(this);
    this.navigationStore = new NavigationStore();
  }
}

const rootStore = new RootStore();

export default rootStore;
