import {createContext, useContext} from 'react';
import rootStore from './RootStore';

const StoreContext = createContext<typeof rootStore | undefined>(undefined);

// create the provider component
export const RootStoreProvider = ({children}: {children: React.ReactNode}) => {
  return (
    <StoreContext.Provider value={rootStore}>{children}</StoreContext.Provider>
  );
};

// create the hook
export const useRootStore = () => {
  const context = useContext(StoreContext);
  if (context === undefined) {
    throw new Error('useRootStore must be used within RootStoreProvider');
  }

  return context;
};
