export const Colors = {
  delftBlue: '#2B3A67',
  payensGray: '#496A81',
  black: '#000',
  white: '#fff',
  antiFlashWhite: '#F2F5F8',
  silverLakeBlue: '#6085A9',
  tomato: '#FB4D3D',
  darkSpreengGreen: '#2C6E49',
  honeydew: '#DFF8EB'
};
