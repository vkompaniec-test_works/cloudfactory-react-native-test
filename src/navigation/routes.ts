export enum TabRoutes {
  'STOCKS_STACK' = 'STOCKS_STACK',
  'ABOUT' = 'ABOUT',
}

export enum StocksRoutes {
  'STOCKS' = 'STOCKS',
  'PAIR_INFO' = 'PAIR_INFO',
}
