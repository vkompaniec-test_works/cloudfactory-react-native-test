import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import StocksScreen from '@screens/StocksScreen';
import PairInfoScreen from '@screens/PairInfoScreen';
import {StocksRoutes} from './routes';
import {StocksStackRoutes} from '../types/tabNavigationTypes';

const Stack = createNativeStackNavigator<StocksStackRoutes>();

export const StocksStack = () => (
  <Stack.Navigator>
    <Stack.Screen name={StocksRoutes.STOCKS} component={StocksScreen} />
    <Stack.Screen name={StocksRoutes.PAIR_INFO} component={PairInfoScreen} />
  </Stack.Navigator>
);
