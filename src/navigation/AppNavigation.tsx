import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import StocksScreen from '@screens/StocksScreen';
import AboutScreen from '@screens/AbountScreen';
import {TabRoutes} from './routes';
import {TabNavigationRoutes} from '../types/tabNavigationTypes';
import {Colors} from '../constants/colors';
import {
  BottomTabButton,
  BottomTabButtonType,
} from '../components/atoms/BottomTabButton';
import {StocksStack} from './StocksStack';
import {useRootStore} from '../stores/RootStoreContext';

const TabStack = createBottomTabNavigator<TabNavigationRoutes>();

const createStocksButton = ({focused}: {focused: boolean}) => (
  <BottomTabButton type={BottomTabButtonType.STOCKS} focused={focused} />
);

const createAboutButton = ({focused}: {focused: boolean}) => (
  <BottomTabButton type={BottomTabButtonType.ABOUT} focused={focused} />
);

export const AppNavigation = () => {
  const {
    navigationStore: {setTabRoute},
  } = useRootStore();
  return (
    <NavigationContainer>
      <TabStack.Navigator
        screenOptions={{
          tabBarStyle: {
            paddingBottom: 16,
            height: 82,
          },
        }}>
        <TabStack.Screen
          name={TabRoutes.STOCKS_STACK}
          component={StocksStack}
          listeners={{
            focus: () => setTabRoute(TabRoutes.STOCKS_STACK),
          }}
          options={{
            headerShown: false,
            tabBarLabel: 'Stocks',
            tabBarActiveTintColor: Colors.delftBlue,
            tabBarInactiveTintColor: Colors.silverLakeBlue,
            tabBarIcon: createStocksButton,
          }}
        />
        <TabStack.Screen
          name={TabRoutes.ABOUT}
          component={AboutScreen}
          listeners={{
            focus: () => setTabRoute(TabRoutes.ABOUT),
          }}
          options={{
            tabBarLabel: 'About',
            tabBarActiveTintColor: Colors.delftBlue,
            tabBarInactiveTintColor: Colors.silverLakeBlue,
            tabBarIcon: createAboutButton,
          }}
        />
      </TabStack.Navigator>
    </NavigationContainer>
  );
};
